const AWS = require('aws-sdk');
AWS.config.update({
    region: "eu-west-1"
});

const dynamo = new AWS.DynamoDB({apiVersion: '2012-10-08'});

exports.handler = function (event, context, callback) {
    dynamo.getItem({
        TableName: 'public-video-videos-db',
        Key: {
            'videoId': {S: event.videoId}
        }
    }, function (err, data) {
        if (err) {
            callback(err);
        } else {
            const video = AWS.DynamoDB.Converter.unmarshall(data.Item);
            if (Object.keys(video).length > 0) {
                context.succeed(video);
            } else {
                callback("No video found with id: [" + event.videoId + "]");
            }
        }
    })
};