const AWS = require('aws-sdk');
AWS.config.update({
    region: "eu-west-1"
});

const transcoder = new AWS.ElasticTranscoder({apiVersion: '2012-09-25'});

const pipelineConfig = {
    pipelineId: "",
    outputKeyPrefix: "vod/",
    segmentDuration: "4.0",
    mpdVideoPreset: "1351620000001-500050",
    mpdAudioPreset: "1351620000001-500060",
    hlsVideoPreset: "1351620000001-200045",
    hlsAudioPreset: "1351620000001-200071"
};

const videoId = "";
/* event.videoId ? */
const inputKey = "";
/* event.inputKey ? */

const params = {
    PipelineId: pipelineConfig.pipelineId,
    Inputs: [
        {
            Key: inputKey
        },
    ],
    OutputKeyPrefix: pipelineConfig.outputKeyPrefix,
    Outputs: [
        {
            Key: videoId + "/mpd/video",
            PresetId: pipelineConfig.mpdVideoPreset,
            SegmentDuration: pipelineConfig.segmentDuration,
        },
        {
            Key: videoId + "/mpd/audio",
            PresetId: pipelineConfig.mpdAudioPreset,
            SegmentDuration: pipelineConfig.segmentDuration,
        },
        {
            Key: videoId + "/hls/video",
            PresetId: pipelineConfig.hlsVideoPreset,
            SegmentDuration: pipelineConfig.segmentDuration,
        },
        {
            Key: videoId + "/hls/audio",
            PresetId: pipelineConfig.hlsAudioPreset,
            SegmentDuration: pipelineConfig.segmentDuration,
        }
    ],
    Playlists: [
        {
            Format: 'MMPEG-DASH',
            Name: videoId + "-mpeg-dash",
            OutputKeys: [videoId + "/mpd/video", videoId + "/mpd/audio"],
        },
        {
            Format: 'HLSv4',
            Name: videoId + "-hls",
            OutputKeys: [videoId + "/hls/video", videoId + "/hls/audio"],
        }
    ],
    UserMetadata: {
        'videoId': videoId
    }
};

exports.handler = function (event, context, callback) {
    transcoder.createJob(params, function (err, data) {
        if (err) {
            callback(err);
        } else {
            context.succeed("Job created for video with id");
        }
    })
};