const AWS = require('aws-sdk');
AWS.config.update({
    region: "eu-west-1"
});

const dynamo = new AWS.DynamoDB({apiVersion: '2012-10-08'});

exports.handler = function (event, context, callback) {
    dynamo.putItem({
        TableName: 'public-video-videos-db',
        Item: AWS.DynamoDB.Converter.marshall({
            videoId: "vid-01",
            source: {
                bucket: "ze-bucket",
                key: "ze-key"
            },
            originInfo: {
                title: "Best test item in the world."
            }
        })
    }, function (err, data) {
        if (err) {
            callback(err);
        } else {
            context.succeed("vid-01");
        }
    })
};